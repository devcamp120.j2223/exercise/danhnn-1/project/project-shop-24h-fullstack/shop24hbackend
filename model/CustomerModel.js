const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const CustomerSchema = new Schema({
  _id: {
    type: mongoose.Types.ObjectId,
  },
  fullName: {
    type: String,
    require: true,
  },
  phone: {
    type: String,
    required: true,
    unique: true,
  },
  email: {
    type: String,
    required: true,
    unique: true,
  },
  address: {
    type: String,
    default: "",
  },
  orders: [
    {
      type: mongoose.Types.ObjectId,
      ref: "Order",
    },
  ],
  timeCreated: {
    type: Date,
    default: Date.now(),
  },
  timeUpdated: {
    type: Date,
    default: Date.now(),
  },
});

//Export
module.exports = mongoose.model("Customer", CustomerSchema);
